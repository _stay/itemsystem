﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace ItemSystem
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API 配置和服务

            // Web API 路由
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{para1}",
                defaults: new { para1 = RouteParameter.Optional }
            );

            config.Routes.MapHttpRoute(
                name: "AddItemApi",
                routeTemplate: "api/AddItem/{jobj}",
                defaults: new object()
            );

            config.Routes.MapHttpRoute(
                name: "StoreItemApi",
                routeTemplate: "api/StoreItem/{jobj}",
                defaults: new object()
            );

            config.Routes.MapHttpRoute(
                name: "TakeItemApi",
                routeTemplate: "api/TakeItem/{jobj}",
                defaults: new object()
            );

            config.Routes.MapHttpRoute(
                name: "RegisterSpaceApi",
                routeTemplate: "api/RegisterSpace/{jobj}",
                defaults: new object()
            );
        }
    }
}
