﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ItemSystem.Models
{
    public class Zone
    {
        public int id { get; set; }
        public string name { get; set; }
        [JsonIgnore]
        public virtual List<Storage> storages { get; set; }
        [JsonIgnore]
        public virtual List<Item> items { get; set; }
    }
}