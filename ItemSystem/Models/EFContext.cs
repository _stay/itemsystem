﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ItemSystem.Models
{
    public class EFContext : DbContext
    {
        public DbSet<Zone> zones { get; set; }
        public DbSet<Storage> storages { get; set; }
        public DbSet<Container> containers { get; set; }
        public DbSet<Item> items { get; set; }
    }
}