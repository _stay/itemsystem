﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ItemSystem.Models
{
    public class Container
    {
        public int id { get; set; }
        public string name { get; set; }
        public string appearance { get; set; }
        [JsonIgnore]
        public virtual List<Item> items { get; set; }
        [Required, JsonIgnore]
        public virtual Storage storage { get; set; }
    }
}