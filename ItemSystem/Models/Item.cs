﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ItemSystem.Models
{
    public class Item
    {
        public int id { get; set; }
        public string name { get; set; }
        public string usage { get; set; }
        public bool isStoring { get; set; }
        public DateTime? storeTime { get; set; }
    }
}