namespace ItemSystem.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Containers",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        name = c.String(),
                        appearance = c.String(),
                        storage_id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Storages", t => t.storage_id, cascadeDelete: true)
                .Index(t => t.storage_id);
            
            CreateTable(
                "dbo.Items",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        name = c.String(),
                        usage = c.String(),
                        isStoring = c.Boolean(nullable: false),
                        storeTime = c.DateTime(),
                        Container_id = c.Int(),
                        Storage_id = c.Int(),
                        Zone_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Containers", t => t.Container_id)
                .ForeignKey("dbo.Storages", t => t.Storage_id)
                .ForeignKey("dbo.Zones", t => t.Zone_id)
                .Index(t => t.Container_id)
                .Index(t => t.Storage_id)
                .Index(t => t.Zone_id);
            
            CreateTable(
                "dbo.Storages",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        name = c.String(),
                        location = c.String(),
                        zone_id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Zones", t => t.zone_id, cascadeDelete: true)
                .Index(t => t.zone_id);
            
            CreateTable(
                "dbo.Zones",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        name = c.String(),
                    })
                .PrimaryKey(t => t.id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Containers", "storage_id", "dbo.Storages");
            DropForeignKey("dbo.Storages", "zone_id", "dbo.Zones");
            DropForeignKey("dbo.Items", "Zone_id", "dbo.Zones");
            DropForeignKey("dbo.Items", "Storage_id", "dbo.Storages");
            DropForeignKey("dbo.Items", "Container_id", "dbo.Containers");
            DropIndex("dbo.Storages", new[] { "zone_id" });
            DropIndex("dbo.Items", new[] { "Zone_id" });
            DropIndex("dbo.Items", new[] { "Storage_id" });
            DropIndex("dbo.Items", new[] { "Container_id" });
            DropIndex("dbo.Containers", new[] { "storage_id" });
            DropTable("dbo.Zones");
            DropTable("dbo.Storages");
            DropTable("dbo.Items");
            DropTable("dbo.Containers");
        }
    }
}
