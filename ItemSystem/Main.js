$(function() {
	$.getJSON('api/zone')
	.done(function (data){
		$.each(data, function(index, val) {
			var zone = $('<p>').text(formatZone(val));
			zone.click(function(event) {
				$.getJSON('api/zone/' + val.id)
				.done(function (data){
					$.each(data, function(index, val) {
						 $('<p>', { text: formatStorage(val) }).appendTo(zone);
					});
				})
			});
			zone.appendTo($('#zones'));
		});
	});
});

function formatZone(item) {
	return item.id + ': ' + item.name;
}

function formatStorage(item) {
	return item.id + ': ' + item.name + '- ' + item.location;
}