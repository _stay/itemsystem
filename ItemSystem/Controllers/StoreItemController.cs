﻿using ItemSystem.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ItemSystem.Controllers
{
    public class StoreItemController : ApiController
    {
        enum Type
        {
            Zone,
            Storage,
            Container,
        }
        public EFContext context = new EFContext();

        [HttpPost]
        public IHttpActionResult StoreItem(JObject jobj)
        {
            if (jobj == null)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("JSON对象为空") });
            }
            var iitemid = jobj.Value<int?>("itemid");
            var itype = jobj.Value<int?>("type");
            var istoreid = jobj.Value<int?>("storeid");
            if (iitemid == null
                || itype == null
                || istoreid == null)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("存放物品信息缺失") });
            }
            var items = from linq_item in context.items
                        where linq_item.id == iitemid.Value
                        select linq_item;
            if (items == null || items.Count() != 1)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("未找到该物品") });
            }
            var item = items.First();
            switch (itype.Value)
            {
                case (int)Type.Zone:
                    var zones = from linq_zone in context.zones
                                where linq_zone.id == istoreid.Value
                                select linq_zone;
                    if (zones == null || zones.Count() != 1)
                    {
                        throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("未找到该区域") });
                    }
                    var zone = zones.First();
                    if (zone.items.Contains(item))
                    {
                        throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.Forbidden) { Content = new StringContent("该区域存在了相同的物品") });
                    }
                    zone.items.Add(item);
                    break;
                case (int)Type.Storage:
                    var storages = from linq_storage in context.zones
                                   where linq_storage.id == istoreid.Value
                                   select linq_storage;
                    if (storages == null || storages.Count() != 1)
                    {
                        throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("未找到该存放点") });
                    }
                    var storage = storages.First();
                    if (storage.items.Contains(item))
                    {
                        throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.Forbidden) { Content = new StringContent("该存放点存在了相同的物品") });
                    }
                    storage.items.Add(item);
                    break;
                case (int)Type.Container:
                    var containers = from linq_container in context.zones
                                     where linq_container.id == istoreid.Value
                                     select linq_container;
                    if (containers == null || containers.Count() != 1)
                    {
                        throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("未找到该容器") });
                    }
                    var container = containers.First();
                    if (container.items.Contains(item))
                    {
                        throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.Forbidden) { Content = new StringContent("该容器存在了相同的物品") });
                    }
                    container.items.Add(item);
                    break;
                default:
                    throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.Forbidden) { Content = new StringContent("未知存放地点类型") });
            }
            item.isStoring = true;
            item.storeTime = DateTime.Now;
            context.SaveChanges();
            return Ok();
        }
    }
}
