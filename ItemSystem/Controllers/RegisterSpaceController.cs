﻿using ItemSystem.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ItemSystem.Controllers
{
    public class RegisterSpaceController : ApiController
    {
        enum Type
        {
            Zone,
            Storage,
            Container,
        }
        public EFContext context = new EFContext();

        [HttpPost]
        public IHttpActionResult RegisterSpace(JObject jobj)
        {
            if (jobj == null)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("JSON对象为空") });
            }
            var itype = jobj.Value<int?>("type");
            if (itype == null)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("存放地点类型未知") });
            }
            switch (itype.Value)
            {
                case (int)Type.Zone:
                    var szname = jobj.Value<string>("name");
                    if (szname == null)
                    {
                        throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("存放地点信息缺失") });
                    }
                    var newZone = new Zone
                    {
                        name = szname,
                    };
                    context.zones.Add(newZone);
                    break;
                case (int)Type.Storage:
                    szname = jobj.Value<string>("name");
                    var szlocation = jobj.Value<string>("location");
                    var izone = jobj.Value<int?>("zone");
                    if (szname == null
                        || szlocation == null
                        || izone == null)
                    {
                        throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("存放地点信息缺失") });
                    }
                    var zones = from linq_zone in context.zones
                                where linq_zone.id == izone.Value
                                select linq_zone;
                    if (zones == null || zones.Count() != 1)
                    {
                        throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("未找到该区域") });
                    }
                    var zone = zones.First();
                    var newStorage = new Storage
                    {
                        name = szname,
                        location = szlocation,
                    };
                    zone.storages.Add(newStorage);
                    break;
                case (int)Type.Container:
                    szname = jobj.Value<string>("name");
                    var szappearance = jobj.Value<string>("appearance");
                    var istorage = jobj.Value<int?>("storage");
                    if (szname == null
                        || szappearance == null
                        || istorage == null)
                    {
                        throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("存放地点信息缺失") });
                    }
                    var storages = from linq_storage in context.storages
                                   where linq_storage.id == istorage.Value
                                   select linq_storage;
                    if (storages == null || storages.Count() != 1)
                    {
                        throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("未找到该存放点") });
                    }
                    var storage = storages.First();
                    var newContainer = new Container
                    {
                        name = szname,
                        appearance = szappearance,
                    };
                    storage.containers.Add(newContainer);
                    break;
                default:
                    throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.Forbidden) { Content = new StringContent("未知存放地点类型") });
            }
            context.SaveChanges();
            return Ok();
        }
    }
}
