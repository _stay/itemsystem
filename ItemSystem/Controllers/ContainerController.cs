﻿using ItemSystem.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ItemSystem.Controllers
{
    public class ContainerController : ApiController
    {
        public EFContext context = new EFContext();

        public IEnumerable<Item> GetAllItem(int para1)
        {
            var containers = from linq_container in context.storages
                          where linq_container.id == para1
                          select linq_container;
            if (containers == null || containers.Count() != 1)
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("未找到该容器") });
            return containers.First().items;
        }
    }
}
