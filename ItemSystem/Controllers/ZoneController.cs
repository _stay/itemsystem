﻿using ItemSystem.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ItemSystem.Controllers
{
    public class ZoneController : ApiController
    {
        public EFContext context = new EFContext();

        public IEnumerable<Zone> GetAllZone()
        {
            return context.zones;
        }

        public IEnumerable<Storage> GetAllStorage(int para1)
        {
            var zones = from linq_zone in context.zones
                       where linq_zone.id == para1
                       select linq_zone;
            if (zones == null || zones.Count() != 1)
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("未找到该区域") });
            return zones.First().storages;
        }
    }
}
