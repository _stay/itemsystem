﻿using ItemSystem.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ItemSystem.Controllers
{
    public class ItemController : ApiController
    {
        public EFContext context = new EFContext();

        public IEnumerable<Item> GetAllLonelyItem()
        {
            return from linq_item in context.items
                   where !context.zones.Any(zone => zone.items.Contains(linq_item))
                   && !context.storages.Any(storage => storage.items.Contains(linq_item))
                   && !context.containers.Any(container => container.items.Contains(linq_item))
                   select linq_item;
        }

        public IHttpActionResult GetItem(int para1)
        {
            if (para1 == 0)
                return Ok(context.items);
            string result = string.Empty;
            var items = from linq_item in context.items
                        where linq_item.id == para1
                        select linq_item;
            if (items == null || items.Count() != 1)
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("未找到该物品") });
            var item = items.First();
            if (!item.isStoring)
                return Ok("该物品未被存放");
            var zones = from linq_zone in context.zones
                        where linq_zone.items.Contains(item)
                        select linq_zone;
            var storages = from linq_storage in context.storages
                           where linq_storage.items.Contains(item)
                           select linq_storage;
            var containers = from linq_container in context.containers
                             where linq_container.items.Contains(item)
                             select linq_container;
            if (zones != null && zones.Count() == 1)
            {
                var zone = zones.First();
                result = string.Format(item.storeTime.HasValue ? "{0}于{1}存放在{2}中" : "{0}存放在{2}中", item.name, item.storeTime, zone.name);
            }
            else if (storages != null && storages.Count() == 1)
            {
                var storage = storages.First();
                result = string.Format(item.storeTime.HasValue ? "{0}于{1}存放在{2}的{3}{4}中" : "{0}存放在{2}的{3}{4}中", item.name, item.storeTime, storage.zone.name, storage.location, storage.name);
            }
            else if (containers != null && containers.Count() == 1)
            {
                var container = containers.First();
                result = string.Format(item.storeTime.HasValue ? "{0}于{1}存放在{2}的{3}{4}中，用{5}的{6}装着" : "{0}存放在{2}的{3}{4}中，用{5}的{6}装着", item.name, item.storeTime, container.storage.zone.name, container.storage.location, container.storage.name, container.appearance, container.name);
            }
            else
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("未找到该物品") });
            }
            return Ok(result);
        }
    }
}
