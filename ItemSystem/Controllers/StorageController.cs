﻿using ItemSystem.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ItemSystem.Controllers
{
    public class StorageController : ApiController
    {
        public EFContext context = new EFContext();

        public IEnumerable<Container> GetAllContainer(int para1)
        {
            var storages = from linq_storage in context.storages
                          where linq_storage.id == para1
                          select linq_storage;
            if (storages == null || storages.Count() != 1)
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("未找到该存放点") });
            return storages.First().containers;
        }
    }
}
